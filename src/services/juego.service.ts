import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { JugadasDTO, JuegoDTO, JuegoPorTurno } from './interface/api.interface';

@Injectable({
  providedIn: 'root',
})
export class JuegoService {
  private url = environment.apiUrl;

  constructor(private _http: HttpClient) {}

  createJuego(newJuego: JuegoDTO): Observable<any> {
    return this._http.post(this.url + '/juego', newJuego);
  }

  getJuegos(): Observable<any> {
    return this._http.get(this.url + '/juego');
  }

  updateJuego(idJuego: string, juego: JuegoDTO) {
    return this._http.put(this.url + '/juego/' + idJuego, juego);
  }

  getJuego(idJuego: string) {
    return this._http.get<JuegoDTO>(this.url + '/juego/' + idJuego);
  }

  createUpdateLastJugada(
    idJuego?: string,
    jugada?: JugadasDTO
  ): Observable<JugadasDTO> {
    console.log(idJuego, jugada);

    return this._http.put(this.url + '/addUltimoJuego/' + idJuego, jugada);
  }

  getLastJuego(idJuego: string): Observable<JugadasDTO> {
    return this._http.get(this.url + '/juego/getLastJuego/' + idJuego);
  }

  deleteJuego(idJuego: string) {
    return this._http.delete(this.url + '/juego/' + idJuego);
  }

  deleteJugada(idJuego: string, fecha: string | Date) {
    const options = { body: { fecha: fecha } };
    return this._http.delete(
      this.url + '/juego/' + idJuego + '/jugada',
      options
    );
  }

  getSoloJuegos(): Observable<any> {
    return this._http.get(this.url + '/soloJuegos');
  }

  getSoloJuego(idJuego: string): Observable<any> {
    return this._http.get(this.url + '/soloJuego/' + idJuego);
  }

  getColeccionPorAño(año: string): Observable<any> {
    return this._http.get(this.url + '/coleccion/?año=' + año);
  }

  getJuegoConJugadasPaginadas(idJuego: string, desde: number, cant: number) {
    return this._http.get<JuegoDTO>(
      this.url + '/juego/jugadasPaginadas/' + idJuego,
      {
        params: { desde: desde, cant: cant },
      }
    );
  }

  getJugadasPaginadas(idJuego: string, desde: number, cant: number) {
    return this._http.get<JuegoDTO>(
      this.url + '/juego/jugadasPaginadas/' + idJuego,
      {
        params: { desde: desde, cant: cant },
      }
    );
  }

  getJuegosLastJugadaPorTurno(turno: string) {
    return this._http.get<JuegoPorTurno>(
      this.url + '/juego/getAllJuego/' + turno
    )
  }

  updateJuegoActualEnCartelera(turno: string) {
    return this._http.put(this.url + '/juego/updateActualJuego/' + turno, {});
  }
}
