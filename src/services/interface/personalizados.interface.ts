export interface PersonalizadosDTO {
    _id?: string;
    parametroId: string;
    tituloUnoCarteleraUno: string;
    tituloDosCarteleraUno: string;
    imagenCarteleraUno?: string;
    tituloUnoCarteleraDos?: string;
    tituloDosCarteleraDos?: string;
    createdAt?: Date;
    updateAt?: Date;
}

export interface PersonalizadosResponseDTO {
  _id: string;
  parametroId: string;
  tituloUnoCarteleraUno: string;
  tituloDosCarteleraUno: string;
  imagenCarteleraUno: string;
  tituloUnoCarteleraDos: string;
  tituloDosCarteleraDos: string;
  editable: boolean;
}