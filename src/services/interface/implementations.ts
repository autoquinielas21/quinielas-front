import { JuegoDTO, JugadasDTO } from './api.interface';

export class JuegoIMPL implements JuegoDTO {
  _id?: string | undefined;
  nombre!: string;
  turno!: string;
  logo!: string;
  horaDelSorteo!: string;
  vigente!: boolean;
  juegoActual?: boolean;
  jugadas!: JugadasDTO[];
  createdAt?: Date | undefined;
  updateAt?: Date | undefined;
}

export class JugadaIMPL implements JugadasDTO {
  fecha?: Date | string;
  puesto1?: number | null;
  puesto2?: number | null;
  puesto3?: number | null;
  puesto4?: number | null;
  puesto5?: number | null;
  puesto6?: number | null;
  puesto7?: number | null;
  puesto8?: number | null;
  puesto9?: number | null;
  puesto10?: number | null;
  puesto11?: number | null;
  puesto12?: number | null;
  puesto13?: number | null;
  puesto14?: number | null;
  puesto15?: number | null;
  puesto16?: number | null;
  puesto17?: number | null;
  puesto18?: number | null;
  puesto19?: number | null;
  puesto20?: number | null;
}
