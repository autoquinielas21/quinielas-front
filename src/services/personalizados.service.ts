import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { PersonalizadosDTO } from "./interface/personalizados.interface";


@Injectable({
    providedIn: 'root',
})
export class PersonalizadosService { 
    private url = environment.apiUrl;

    constructor(private _http: HttpClient) {}

    getPersonalizados(): Observable<any> {
        return this._http.get(this.url + '/getPersonalizado');
    }

    putPersonalizado(personalizado: PersonalizadosDTO): Observable<any> {
        return this._http.put(this.url + '/putPersonalizado', personalizado);
    }

    deletePersonalizado(id: string): Observable<any> {
        return this._http.delete(this.url + '/deletePersonalizado/' + id);
    }

    getPersonalizadoByParametroId(parametroId: string): Observable<any> {
        return this._http.get(this.url + '/getPersonalizado/' + parametroId);
    }
}
      