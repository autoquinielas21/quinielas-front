import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NetworkStatusComponent } from '../network-status/network-status.component';
import { AddZeroPipe } from '../pipe/add-zero.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [AddZeroPipe, NetworkStatusComponent],
  exports: [AddZeroPipe, CommonModule, NetworkStatusComponent],
})
export class SharedModule {}
