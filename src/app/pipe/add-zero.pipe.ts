import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addZero',
})
export class AddZeroPipe implements PipeTransform {
  transform(value: number | null | undefined, ...args: unknown[]): string {
    let maxLenght = 4;
    let zero = '0';

    if (typeof value === 'number') {
      if (value.toString().length < maxLenght) {
        return (
          zero.repeat(maxLenght - value.toString().length) + value.toString()
        );
      }
    } else {
      return '';
    }

    return value.toString();
  }
}
