import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VeinteComponent } from './cartelera/veinte/veinte.component';
import { PrimerosComponent } from './cartelera/vista-primeros-todos/primeros.component';
import { PrimerosTurnosComponent } from './cartelera/vista-primeros-turnos/primeros-turnos.component';
import { JugadasEditTurnosComponent } from './cont-panel/jugadas-edit-turnos/jugadas-edit-turnos.component';
import { PersonalizadosComponent } from './cont-panel/personalizados/personalizados.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/cartelera',
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./cont-panel/cont-panel.module').then((m) => m.ContPanelModule),
  },
  {
    path: 'cartelera',
    component: VeinteComponent,
    loadChildren: () =>
      import('./cartelera/cartelera.module').then((m) => m.CarteleraModule),
  },
  {
    path: 'primeros',
    component: PrimerosComponent,
    loadChildren: () =>
      import('./cartelera/cartelera.module').then((m) => m.CarteleraModule),
  },
  {
    path: 'turnos',
    component: PrimerosTurnosComponent,
    loadChildren: () =>
      import('./cartelera/cartelera.module').then((m) => m.CarteleraModule),
  },
  {
    path: 'admin/editTurno/:turno',
    component: JugadasEditTurnosComponent,
    loadChildren: () =>
      import('./cont-panel/cont-panel.module').then((m) => m.ContPanelModule)
  },
  {
    path: 'admin/personalizados',
    component: PersonalizadosComponent,
    loadChildren: () => 
      import('./cont-panel/cont-panel.module').then((m) => m.ContPanelModule)
  },
  {
    path: '**',
    redirectTo: '/cartelera',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
