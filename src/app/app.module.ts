import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DateService } from 'src/services/date.service';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule, 
    NgbModule, 
    AppRoutingModule, 
    ToastrModule.forRoot(), 
    BrowserAnimationsModule,
  ],
  providers: [DateService],
  bootstrap: [AppComponent],
})
export class AppModule {}
