import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { JuegoPorTurno } from "src/services/interface/api.interface";
import { JuegoService } from "src/services/juego.service";
import { Location } from '@angular/common';

@Component({
    selector: 'app-jugadas-edit-turnos',
    templateUrl: './jugadas-edit-turnos.component.html',
    styleUrls: ['./jugadas-edit-turnos.component.scss'],
}) 
export class JugadasEditTurnosComponent implements OnInit {

    turno?: string = '';
    juegos: JuegoPorTurno[] = [];

    constructor(
        private _router: Router, 
        private _aRoute: ActivatedRoute,
        private _juegoService: JuegoService,
        private _location: Location
    ) {}

    ngOnInit(): void {
        this._aRoute.params.subscribe((params: Params) => this.turno = params['turno']);
        this.getJuegos();
    }

    getJuegos() {
        this._juegoService.getJuegosLastJugadaPorTurno(this.turno!).subscribe((data: any) => {
            let dataAux: JuegoPorTurno[] = [];
            data.forEach((x: JuegoPorTurno) => {
                if (x.nombre?.toLocaleUpperCase().includes('CIUDAD')) 
                    dataAux[0] = x;
                if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA')) 
                    dataAux[1] = x;
                if (x.nombre?.toLocaleUpperCase().includes('CORDOBA')) 
                    dataAux[2] = x;
                if (x.nombre?.toLocaleUpperCase().includes('SANTA FE')) 
                    dataAux[3] = x;
                if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS')) 
                    dataAux[4] = x;
                if (x.nombre?.toLocaleUpperCase().includes('MONTEVIDEO')) 
                    dataAux[5] = x;
            });
            dataAux.map((x: JuegoPorTurno) => this.juegos.push(x));
            
        })
    }
      
    back() {
        this._location.back();
    }
}