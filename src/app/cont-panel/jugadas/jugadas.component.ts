import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { JuegoDTO, JugadasDTO } from 'src/services/interface/api.interface';
import { JugadaIMPL } from 'src/services/interface/implementations';
import { JuegoService } from 'src/services/juego.service';
import { sys } from 'typescript';

@Component({
  selector: 'app-jugadas',
  templateUrl: './jugadas.component.html',
  styleUrls: ['./jugadas.component.scss'],
})
export class JugadasComponent implements OnInit, OnChanges {
  fechaActual = new Date();
  @Input() juego!: JuegoDTO;
  formCrear: FormGroup = new FormGroup({
    dia: new FormControl(this.fechaActual.getDate()),
    mes: new FormControl(this.fechaActual.getMonth() + 1),
    anno: new FormControl(this.fechaActual.getFullYear()),
  });
  jugadas: JugadasDTO[] = [];
  proximasJugadas: boolean = false;
  paginaActual: number = 0;

  constructor(private _juegos: JuegoService) {}

  ngOnInit(): void {
    if (this.juego) {
      this.reiniciar();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.juego) {
      this.juego = changes.juego.currentValue;
      this.reiniciar();
    }
  }

  private reiniciar() {
    this.jugadas = [];
    this.paginaActual = 0;
    this.paginarJugadas(0);
  }

  botonCrear() {
    var dia = this.formCrear.get('dia')?.value;
    var mes = this.formCrear.get('mes')?.value;
    var anno = this.formCrear.get('anno')?.value;
    if (this.fechaEsValida(dia, mes, anno)) {
      var jugadaNueva = new JugadaIMPL();
      jugadaNueva.fecha = new Date(anno, mes - 1, dia);
      this._juegos
        .createUpdateLastJugada(this.juego._id, jugadaNueva)
        .subscribe({
          next: (a: JugadasDTO) => {
            this.jugadas = [];
            this.paginarJugadas(0);
          },
        });
    }
  }

  botonCargarMasJugadas() {
    this.paginaActual += 1;
    this.paginarJugadas(this.paginaActual);
  }

  private paginarJugadas(pagina: number) {
    console.log({ log: 'paginando', pag: pagina });
    const cant = 10;
    const desde = pagina * cant;
    this._juegos
      .getJuegoConJugadasPaginadas(this.juego._id!, desde, cant)
      .subscribe({
        next: (b: JuegoDTO) => {
          b.jugadas.forEach((j) => this.agregarJugadaSoloSiNoExiste(j));
        },
        complete: () => {
          console.log({ log: 'Finished first query' });
          this._juegos
            .getJuegoConJugadasPaginadas(this.juego._id!, desde + cant, 1)
            .subscribe({
              next: (r) => {
                this.proximasJugadas =
                  r.jugadas != null && r.jugadas.length > 0;
              },
              complete: () => {
                console.log({ log: 'finished second query' });
              },
              error: () => {
                console.log('Nothing more'), (this.proximasJugadas = false);
              },
            });
        },
        error: () => console.log('Nothing more'),
      });
  }

  agregarJugadaSoloSiNoExiste(jugada: JugadasDTO) {
    var existente = this.jugadas.filter(
      (j) => jugada.fecha != null && j.fecha == jugada.fecha
    );
    console.log({ log: 'jugada existente', value: existente });
    if (existente.length == 0) {
      this.jugadas.push(jugada);
    }
  }

  fechaEsValida(dia: number, mes: number, anno: number): boolean {
    if (anno < 2000 || anno > 2100) {
      alert('El año seleccionado no es válido');
      return false;
    }
    if (mes < 1 || mes > 12) {
      alert('El mes seleccionado no es válido');
      return false;
    } else {
      if ([1, 3, 5, 7, 8, 10, 12].includes(mes)) {
        if (dia < 1 || dia > 31) {
          alert('Día inválido');
          return false;
        }
      } else {
        if ([2].includes(mes)) {
          if (dia < 1 || dia > 29) {
            alert('Día inválido');
            return false;
          }
        } else {
          if (dia < 1 || dia > 30) {
            alert('Día inválido');
            return false;
          }
        }
      }
    }
    return true;
  }

  manejarEventos(evento: string) {
    if ('jugada-eliminada') {
      this.reiniciar();
    }
  }
}
