import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { PersonalizadosDTO, PersonalizadosResponseDTO } from "src/services/interface/personalizados.interface";
import { PersonalizadosService } from "src/services/personalizados.service";
import { Location } from '@angular/common';

@Component({
    selector: 'app-personalizados',
    templateUrl: './personalizados.component.html',
    styleUrls: ['./personalizados.component.scss'],
})
export class PersonalizadosComponent implements OnInit {
    personalizados: PersonalizadosResponseDTO[] = [];
    myForm: FormGroup = this.initForm();

    constructor(private _ps: PersonalizadosService, private _location: Location) {}

    ngOnInit(): void {
        this.getPersonalizados();
    }

    initForm() {
        return new FormGroup({
            parametroId: new FormControl('', Validators.required),
            tituloUnoCarteleraUno: new FormControl(''),
            tituloDosCarteleraUno: new FormControl(''),
            imagenCarteleraUno: new FormControl(''),
            tituloUnoCarteleraDos: new FormControl(''),
            tituloDosCarteleraDos: new FormControl(''),
        });
    }

    getPersonalizados() {
        this.personalizados = [];
        this._ps.getPersonalizados().subscribe((data: PersonalizadosResponseDTO[]) => {
            console.log(data);
            data.forEach((d: PersonalizadosResponseDTO) => {
                this.personalizados.push({
                    _id: d._id,
                    parametroId: d.parametroId,
                    tituloUnoCarteleraUno: d.tituloUnoCarteleraUno,
                    tituloDosCarteleraUno: d.tituloDosCarteleraUno,
                    imagenCarteleraUno: d.imagenCarteleraUno,
                    tituloUnoCarteleraDos: d.tituloUnoCarteleraDos,
                    tituloDosCarteleraDos: d.tituloDosCarteleraDos,
                    editable: true
                });
            })
        });
    }

    limpiarValores() {
        this.myForm.get('parametroId')?.setValue('');
        this.myForm.get('tituloUnoCarteleraUno')?.setValue('');
        this.myForm.get('tituloDosCarteleraUno')?.setValue('');
        this.myForm.get('imagenCarteleraUno')?.setValue('');
        this.myForm.get('tituloUnoCarteleraDos')?.setValue('');
        this.myForm.get('tituloDosCarteleraDos')?.setValue('');
    }

    newPersonalizado() {
        let newPersonalizado: PersonalizadosDTO = {
            parametroId: this.myForm.get('parametroId')?.value,
            tituloUnoCarteleraUno: this.myForm.get('tituloUnoCarteleraUno')?.value,
            tituloDosCarteleraUno: this.myForm.get('tituloDosCarteleraUno')?.value,
            imagenCarteleraUno: this.myForm.get('imagenCarteleraUno')?.value ? this.myForm.get('imagenCarteleraUno')?.value : null,
            tituloUnoCarteleraDos: this.myForm.get('tituloUnoCarteleraDos')?.value ? this.myForm.get('tituloUnoCarteleraDos')?.value : null,
            tituloDosCarteleraDos: this.myForm.get('tituloDosCarteleraDos')?.value ? this.myForm.get('tituloDosCarteleraDos')?.value : null
        }

        this._ps.putPersonalizado(newPersonalizado).subscribe(() => {
            this.getPersonalizados();
            this.limpiarValores();
        });
    }

    deletePersonalizado(id: any, nombre: any) {
        let r = confirm(`¿Seguro que desea eliminar esta Cartelera Personalizada con parametro id: ${nombre}?`);

        if (r) {
            this._ps.deletePersonalizado(id).subscribe(() => {
                this.getPersonalizados();
            });
        } 
    }

    changeState(personalizado: PersonalizadosResponseDTO, state: boolean) {
        personalizado.editable = state;
    }

    updatePersonalizado(per: PersonalizadosResponseDTO) {
        let updatePersonalizado: PersonalizadosDTO = {
            parametroId: per.parametroId,
            tituloUnoCarteleraUno: per.tituloUnoCarteleraUno,
            tituloDosCarteleraUno: per.tituloDosCarteleraUno,
            imagenCarteleraUno: per.imagenCarteleraUno,
            tituloUnoCarteleraDos: per.tituloUnoCarteleraDos,
            tituloDosCarteleraDos: per.tituloDosCarteleraDos
        };

        // if (
        //     updatePersonalizado.parametroId === '' || 
        //     updatePersonalizado.tituloUnoCarteleraUno === '' || 
        //     updatePersonalizado.tituloDosCarteleraUno === ''
        // ) {
        //     alert('Los valores \n Parametro ID \n Titulo Principal Cartelera Personalizada \n Titulo Secundario Cartelera Personalizada \n No pueden quedar vacios!');
        // } else {
            
        this._ps.putPersonalizado(updatePersonalizado).subscribe(() => {
            this.getPersonalizados();
        });
        // }
    }

    back() {
        this._location.back();
    }
}