import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadaItemComponent } from './jugada-item.component';

describe('JugadaItemComponent', () => {
  let component: JugadaItemComponent;
  let fixture: ComponentFixture<JugadaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JugadaItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
