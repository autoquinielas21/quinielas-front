import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JuegoDTO, JuegoPorTurno, JugadasDTO } from 'src/services/interface/api.interface';
import { JugadaIMPL } from 'src/services/interface/implementations';
import { JuegoService } from 'src/services/juego.service';

@Component({
  selector: 'app-jugada-item',
  templateUrl: './jugada-item.component.html',
  styleUrls: ['./jugada-item.component.scss'],
})
export class JugadaItemComponent implements OnInit {
  @Input() jugada!: JugadasDTO;
  @Input() juego!: JuegoDTO | JuegoPorTurno;
  @Input() title?: string;
  @Output() emisor = new EventEmitter<string>();
  form!: FormGroup;
  editable = false;
  key: any;
  turno?: string = '';
  flagShowToast = 0;

  constructor(
    private _juegos: JuegoService, 
    private cdr: ChangeDetectorRef, 
    private _aRoute: ActivatedRoute, 
    private toastr: ToastrService
  ) {
    this.toastr.toastrConfig.timeOut = 2500
  }

  ngOnInit(): void {
    this._aRoute.params.subscribe((params: Params) => this.turno = params['turno']);
    this.configFormGroup();
  }

  getFechaFormateada() {
    var fecha = new Date(this.jugada.fecha!);
    return fecha;
  }

  jugClicked() {
    if (!this.editable) {
      this.editable = true;
      this.form.enable();
    }
  }

  guardar() {
    var nueva = new JugadaIMPL();

    nueva.fecha = this.jugada.fecha;
    for (var i = 1; i <= 20; i++) {
      var p = Number(
        this.form.get('puesto' + i)?.value === ''
          ? '-1'
          : this.form.get('puesto' + i)?.value
      );
      if (!Number.isNaN(p)) {
        if (p < 0 || p > 9999) {
          Reflect.set(nueva, 'puesto' + i, null);
        } else {
          Reflect.set(nueva, 'puesto' + i, p);
        }
      }
    }
    this.jugada = nueva;
    this._juegos.createUpdateLastJugada(this.juego._id!, nueva).subscribe({
      next: (a) => console.log(a),
      complete: () => {
        this.configFormGroup();
        this.descartar();
        this.toastr.success('Juego de ' + this.title, 'Guardado');
      },
      error: () => {
        this.toastr.error('Juego de ' + this.title, 'Error');
      }
    });
  }

  descartar() {
    setTimeout(() => {
      this.editable = false;
      this.form.disable();
      this.configFormGroup();
    }, 50);
  }

  eliminar() {
    if (
      confirm(
        '¿Está seguro de querer eliminar esta jugada del día' +
          this.getFechaFormateada() +
          '?'
      )
    ) {
      this._juegos.deleteJugada(this.juego._id!, this.jugada.fecha!).subscribe({
        next: (a) => {
          this.emisor.emit('jugada-eliminada');
          console.log(a);
        },
      });
    }
  }

  configFormGroup() {
    this.form = new FormGroup({
      fecha: new FormControl({
        value: this.getFechaFormateada(),
        disabled: !this.editable,
      }),
      puesto1: new FormControl({
        value:
          this.jugada.puesto1 ||
          (!Number.isNaN(this.jugada.puesto1) && this.jugada.puesto1 === 0)
            ? this.jugada.puesto1!
            : '',
        disabled: !this.editable,
      }),
      puesto2: new FormControl({
        value:
          this.jugada.puesto2 ||
          (!Number.isNaN(this.jugada.puesto2) && this.jugada.puesto2 === 0)
            ? this.jugada.puesto2!
            : '',
        disabled: !this.editable,
      }),
      puesto3: new FormControl({
        value:
          this.jugada.puesto3 ||
          (!Number.isNaN(this.jugada.puesto3) && this.jugada.puesto3 === 0)
            ? this.jugada.puesto3
            : '',
        disabled: !this.editable,
      }),
      puesto4: new FormControl({
        value:
          this.jugada.puesto4 ||
          (!Number.isNaN(this.jugada.puesto4) && this.jugada.puesto4 === 0)
            ? this.jugada.puesto4
            : '',
        disabled: !this.editable,
      }),
      puesto5: new FormControl({
        value:
          this.jugada.puesto5 ||
          (!Number.isNaN(this.jugada.puesto5) && this.jugada.puesto5 === 0)
            ? this.jugada.puesto5
            : '',
        disabled: !this.editable,
      }),
      puesto6: new FormControl({
        value:
          this.jugada.puesto6 ||
          (!Number.isNaN(this.jugada.puesto6) && this.jugada.puesto6 === 0)
            ? this.jugada.puesto6
            : '',
        disabled: !this.editable,
      }),
      puesto7: new FormControl({
        value:
          this.jugada.puesto7 ||
          (!Number.isNaN(this.jugada.puesto7) && this.jugada.puesto7 === 0)
            ? this.jugada.puesto7
            : '',
        disabled: !this.editable,
      }),
      puesto8: new FormControl({
        value:
          this.jugada.puesto8 ||
          (!Number.isNaN(this.jugada.puesto8) && this.jugada.puesto8 === 0)
            ? this.jugada.puesto8
            : '',
        disabled: !this.editable,
      }),
      puesto9: new FormControl({
        value:
          this.jugada.puesto9 ||
          (!Number.isNaN(this.jugada.puesto9) && this.jugada.puesto9 === 0)
            ? this.jugada.puesto9
            : '',
        disabled: !this.editable,
      }),
      puesto10: new FormControl({
        value:
          this.jugada.puesto10 ||
          (!Number.isNaN(this.jugada.puesto10) && this.jugada.puesto10 === 0)
            ? this.jugada.puesto10
            : '',
        disabled: !this.editable,
      }),
      puesto11: new FormControl({
        value:
          this.jugada.puesto11 ||
          (!Number.isNaN(this.jugada.puesto11) && this.jugada.puesto11 === 0)
            ? this.jugada.puesto11
            : '',
        disabled: !this.editable,
      }),
      puesto12: new FormControl({
        value:
          this.jugada.puesto12 ||
          (!Number.isNaN(this.jugada.puesto12) && this.jugada.puesto12 === 0)
            ? this.jugada.puesto12
            : '',
        disabled: !this.editable,
      }),
      puesto13: new FormControl({
        value:
          this.jugada.puesto13 ||
          (!Number.isNaN(this.jugada.puesto13) && this.jugada.puesto13 === 0)
            ? this.jugada.puesto13
            : '',
        disabled: !this.editable,
      }),
      puesto14: new FormControl({
        value:
          this.jugada.puesto14 ||
          (!Number.isNaN(this.jugada.puesto14) && this.jugada.puesto14 === 0)
            ? this.jugada.puesto14
            : '',
        disabled: !this.editable,
      }),
      puesto15: new FormControl({
        value:
          this.jugada.puesto15 ||
          (!Number.isNaN(this.jugada.puesto15) && this.jugada.puesto15 === 0)
            ? this.jugada.puesto15
            : '',
        disabled: !this.editable,
      }),
      puesto16: new FormControl({
        value:
          this.jugada.puesto16 ||
          (!Number.isNaN(this.jugada.puesto16) && this.jugada.puesto16 === 0)
            ? this.jugada.puesto16
            : '',
        disabled: !this.editable,
      }),
      puesto17: new FormControl({
        value:
          this.jugada.puesto17 ||
          (!Number.isNaN(this.jugada.puesto17) && this.jugada.puesto17 === 0)
            ? this.jugada.puesto17
            : '',
        disabled: !this.editable,
      }),
      puesto18: new FormControl({
        value:
          this.jugada.puesto18 ||
          (!Number.isNaN(this.jugada.puesto18) && this.jugada.puesto18 === 0)
            ? this.jugada.puesto18
            : '',
        disabled: !this.editable,
      }),
      puesto19: new FormControl({
        value:
          this.jugada.puesto19 ||
          (!Number.isNaN(this.jugada.puesto19) && this.jugada.puesto19 === 0)
            ? this.jugada.puesto19
            : '',
        disabled: !this.editable,
      }),
      puesto20: new FormControl({
        value:
          this.jugada.puesto20 ||
          (!Number.isNaN(this.jugada.puesto20) && this.jugada.puesto20 === 0)
            ? this.jugada.puesto20
            : '',
        disabled: !this.editable,
      }),
    });
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    this.key = event.key;
    if (this.turno !== undefined) {
      if (this.key === 'g') {
        this.guardar();    
      }
    }
  }
}
