import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { timer } from 'rxjs';
import { JuegoDTO } from 'src/services/interface/api.interface';
import { JuegoService } from 'src/services/juego.service';

@Component({
  selector: 'app-juego-detail',
  templateUrl: './juego-detail.component.html',
  styleUrls: ['./juego-detail.component.scss'],
})
export class JuegoDetailComponent implements OnInit, OnChanges {
  @Input() juego!: JuegoDTO;
  @Input() editable: boolean = false;
  @Input() modo: string = '';
  @Output() emisor = new EventEmitter<string>();
  formcont: FormGroup = this.formcontOf(this.juego);
  turnoElegido: any;
  turnos = ['La Previa', 'Primero', 'Matutina', 'Vespertina', 'Nocturna', '(Ignorar)'];

  constructor(private _juegos: JuegoService) {}

  ngOnInit(): void {
    this.getJuegos();
  }

  // Se triggerea cada vez que una
  // propiedad compartida (p.ej  un @Input) cambia de valor
  ngOnChanges(changes: SimpleChanges) {
    if (changes.juego) {
      this.juego = changes.juego.currentValue;
      this.getJuegos();
    }
    console.log(this.turnoElegido);
  }

  getJuegos() {
    this._juegos.getSoloJuego(this.juego._id!).subscribe({
      next: (js) => {
        this.juego = js;
        this.llenarForm(this.juego);
      },
    });
  }

  llenarForm(income: JuegoDTO) {
    console.log(income);
    this.formcont = this.formcontOf(income);
  }

  private formcontOf(income?: JuegoDTO): FormGroup {
    return new FormGroup({
      nombre: new FormControl(income? income.nombre:''),
      turno: new FormControl(income? income.turno:''),
      logo: new FormControl(income? income.logo:''),
      hora: new FormControl(income? income.horaDelSorteo:''),
      vigente: new FormControl(income? income.vigente:''),
      juegoActual: new FormControl(income? income.juegoActual:''),
    });
  }

  crear() {
    if (confirm('¿Crear nuevo juego con estos datos?')) {
      this.juego.nombre = this.formcont.get('nombre')?.value;
      this.juego.turno = this.formcont.get('turno')?.value;
      this.juego.logo = this.formcont.get('logo')?.value;
      this.juego.horaDelSorteo = this.formcont.get('hora')?.value;
      this.juego.vigente = this.formcont.get('vigente')?.value;
      this._juegos.createJuego(this.juego).subscribe({
        next: (r) => console.log({ message: 'created', response: r }),
      });
    }
    this.emisor.emit('creado');
  }

  guardar() {
    if (confirm('¿Está seguro de guardar estos datos?')) {
      this.juego.nombre = this.formcont.get('nombre')?.value;
      this.juego.turno = this.formcont.get('turno')?.value;
      this.juego.logo = this.formcont.get('logo')?.value;
      this.juego.horaDelSorteo = this.formcont.get('hora')?.value;
      this.juego.vigente = this.formcont.get('vigente')?.value;
      this.juego.juegoActual = this.formcont.get('juegoActual')?.value;
      this._juegos.updateJuego('' + this.juego._id, this.juego).subscribe({
        next: (r) => console.log({ message: 'modified', response: r }),
      });
    }
    this.emisor.emit('editado');
  }

  eliminar() {
    if (confirm('¿Está seguro de eliminar este juego?')) {
      var resp = prompt(
        'Al aceptar este mensaje entiendo que eliminar este juego con todas sus respectivas jugadas es una acción irreversible.\nEsta información no se podrá recuperar ni siquiera llamando a un servicio técnico\n\nSi está seguro de realizar esta acción, escriba "ELIMINAR" (sin comillas)'
      );
      if (resp != null && resp.toLowerCase().trim() == 'eliminar') {
        this._juegos.deleteJuego(this.juego._id!).subscribe({
          next: (r) => console.log({ message: 'deleted', response: r }),
        });
        this.emisor.emit('eliminado');
      }
    }
  }

  descartar() {
    if (confirm('¿Descartar cambios?')) {
      if (this.modo == 'editar') {
        this._juegos.getJuego('' + this.juego._id).subscribe({
          next: (j: JuegoDTO) => {
            this.juego = j;
            this.llenarForm(j);
          },
        });
      }
      this.emisor.emit('descartado');
    }
  }
}
