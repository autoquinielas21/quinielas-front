import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JuegoDTO } from 'src/services/interface/api.interface';
import { JuegoIMPL } from 'src/services/interface/implementations';
import { JuegoService } from 'src/services/juego.service';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.scss'],
})
export class AdminViewComponent implements OnInit {
  juegos: JuegoDTO[] = [];
  juegoSeleccionado?: JuegoDTO;
  mode: string = '';
  queryComplete = false;

  constructor(private _juegos: JuegoService, private _router: Router) {}

  ngOnInit(): void {
    this.getJuegos();
  }

  private getJuegos() {
    this.queryComplete = false;
    this._juegos.getSoloJuegos().subscribe({
      next: (soloJuegos) => {
        if (soloJuegos.length == 27) {
          soloJuegos.forEach((x: any) => {
              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD') && x.turno == 'La Previa') 
                this.juegos[0] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA') && x.turno == 'La Previa') 
                this.juegos[1] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA') && x.turno == 'La Previa') 
                this.juegos[2] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE') && x.turno == 'La Previa') 
                this.juegos[3] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS') && x.turno == 'La Previa') 
                this.juegos[4] = x;

              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD') && x.turno == 'Primero') 
                this.juegos[5] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA') && x.turno == 'Primero') 
                this.juegos[6] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA') && x.turno == 'Primero') 
                this.juegos[7] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE') && x.turno == 'Primero') 
                this.juegos[8] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS') && x.turno == 'Primero') 
                this.juegos[9] = x;
              
              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD') && x.turno == 'Matutina') 
                this.juegos[10] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA') && x.turno == 'Matutina') 
                this.juegos[11] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA') && x.turno == 'Matutina') 
                this.juegos[12] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE') && x.turno == 'Matutina') 
                this.juegos[13] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS') && x.turno == 'Matutina') 
                this.juegos[14] = x;
              if (x.nombre?.toLocaleUpperCase().includes('MONTEVIDEO') && x.turno == 'Matutina') 
                this.juegos[15] = x;

              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD') && x.turno == 'Vespertina') 
                this.juegos[16] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA') && x.turno == 'Vespertina') 
                this.juegos[17] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA') && x.turno == 'Vespertina') 
                this.juegos[18] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE') && x.turno == 'Vespertina') 
                this.juegos[19] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS') && x.turno == 'Vespertina') 
                this.juegos[20] = x;

              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD') && x.turno == 'Nocturna') 
                this.juegos[21] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA') && x.turno == 'Nocturna') 
                this.juegos[22] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA') && x.turno == 'Nocturna') 
                this.juegos[23] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE') && x.turno == 'Nocturna') 
                this.juegos[24] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS') && x.turno == 'Nocturna') 
                this.juegos[25] = x;
              if (x.nombre?.toLocaleUpperCase().includes('MONTEVIDEO') && x.turno == 'Nocturna') 
                this.juegos[26] = x;
          })        
        } else {
          this.juegos = soloJuegos;
        }
      },
      complete: () => {
        this.queryComplete = true;
        console.log(this.juegos.length);
        
      },
    });
  }

  onJuegoClicked(item: JuegoDTO) {
    this.juegoSeleccionado = item;
    this.mode = 'editar';
  }

  onCrearJuegoClicked() {
    this.juegoSeleccionado = new JuegoIMPL();
    this.mode = 'crear';
  }

  manejarEventoDetalle(mensaje: string) {
    if (mensaje == 'descartado' || mensaje == 'editado') {
      this._juegos.getSoloJuego(this.juegoSeleccionado?._id!).subscribe({
        next: (j) => (this.juegoSeleccionado = j),
      });
    }
    if (mensaje == 'creado' || mensaje == 'eliminado' || mensaje == 'editado') {
      this.mode = '';
      this.juegos = [];
      setTimeout(() => this.getJuegos(), 1000);
    }
    if (mensaje == 'creado') {
      this.mode = 'editar';
    }
  }

  goTurnosEdit(turno: string) {
    this._router.navigate([`admin/editTurno`, turno])
  }

  goPersonalizados() {
    this._router.navigate(['admin/personalizados'])
  }
}
