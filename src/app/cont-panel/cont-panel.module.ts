import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { RouterModule, Routes } from '@angular/router';
import { JuegoDetailComponent } from './juego-detail/juego-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JugadasComponent } from './jugadas/jugadas.component';
import { JugadaItemComponent } from './jugada-item/jugada-item.component';
import { JugadasEditTurnosComponent } from './jugadas-edit-turnos/jugadas-edit-turnos.component';
import { PersonalizadosComponent } from './personalizados/personalizados.component';
import { SharedModule } from '../shared/shared.module';


const routes: Routes = [
  {
    path: '',
    component: AdminViewComponent,
  }
];

@NgModule({
  declarations: [
    AdminViewComponent,
    JuegoDetailComponent,
    JugadasComponent,
    JugadaItemComponent,
    JugadasEditTurnosComponent,
    PersonalizadosComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    AdminViewComponent
  ],
})
export class ContPanelModule {}
