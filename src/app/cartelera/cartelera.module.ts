import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VeinteComponent } from './veinte/veinte.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimerosComponent } from './vista-primeros-todos/primeros.component';
import { PrimerosTurnosComponent } from './vista-primeros-turnos/primeros-turnos.component';
import { PersonalizadoComponent } from './personalizado/personalizado.component';
import { PersonalizadoDosComponent } from './personalizadoOpcional/personalizadoDos.component';

const routes: Routes = [
  { path: '', component: VeinteComponent },
  { path: '', component: PrimerosComponent },
  { path: '', component: PrimerosTurnosComponent },
];

@NgModule({
  declarations: [VeinteComponent, PrimerosComponent, PrimerosTurnosComponent, PersonalizadoComponent, PersonalizadoDosComponent],
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  exports: [SharedModule],
})
export class CarteleraModule {}
