import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JuegoDTO } from 'src/services/interface/api.interface';
import { JuegoService } from 'src/services/juego.service';

@Component({
  selector: 'app-primeros',
  templateUrl: './primeros.component.html',
  styleUrls: ['./primeros.component.scss'],
})
export class PrimerosComponent implements OnInit, OnDestroy {
  changeVeinteInterval: any;
  timeChangeVeinteInterval = 1;

  juegos: JuegoDTO[] = [];

  primerCdadPrimera?: number | null;
  primerCdadMatutina?: number | null;
  primerCdadVespertina?: number | null;
  primerCdadNocturna?: number | null;

  primerProvPrimera?: number | null;
  primerProvMatutina?: number | null;
  primerProvVespertina?: number | null;
  primerProvNocturna?: number | null;

  constructor(private _route: Router, private _juegos: JuegoService) {}

  ngOnDestroy(): void {
    clearInterval(this.changeVeinteInterval);
  }

  ngOnInit(): void {
    this.cambiarVeinte();
    this.getJuegos();
  }

  cambiarVeinte() {
    this.changeVeinteInterval = setInterval(() => {
      this._route.navigate(['/cartelera']);
    }, this.timeChangeVeinteInterval * 60000);
  }

  getJuegos() {
    this._juegos.getSoloJuegos().subscribe((data) => {
      this.juegos.push(...data);
      this.getSoloPrimeros();
    });
  }

  getSoloPrimeros() {
    this.juegos.forEach((juego) => {
      if (
        juego.turno.toUpperCase() === 'PRIMERO' ||
        juego.turno.toUpperCase() === 'PRIMERA'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerCdadPrimera = data.puesto1;
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerProvPrimera = data.puesto1;
          });
        } else {
          console.log('aun no existe ese juego....');
        }
      } else if (
        juego.turno.toUpperCase() === 'MATUTINA' ||
        juego.turno.toUpperCase() === 'MATUTINO'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerCdadMatutina = data.puesto1;
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerProvMatutina = data.puesto1;
          });
        } else {
          console.log('aun no existe ese juego....');
        }
      } else if (
        juego.turno.toUpperCase() === 'VESPERTINA' ||
        juego.turno.toUpperCase() === 'VESPERTINO'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerCdadVespertina = data.puesto1;
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerProvVespertina = data.puesto1;
          });
        } else {
          console.log('aun no existe ese juego....');
        }
      } else if (
        juego.turno.toUpperCase() === 'NOCTURNA' ||
        juego.turno.toUpperCase() === 'NOCTURNO'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerCdadNocturna = data.puesto1;
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegos.getLastJuego(juego._id!).subscribe((data) => {
            this.primerProvNocturna = data.puesto1;
          });
        } else {
          console.log('aun no existe ese juego....');
        }
      }
    });
  }
}
