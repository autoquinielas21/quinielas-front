import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DateService } from 'src/services/date.service';
import { JuegoDTO } from 'src/services/interface/api.interface';
import { JuegoService } from 'src/services/juego.service';

@Component({
  selector: 'app-primeros-turnos',
  templateUrl: './primeros-turnos.component.html',
  styleUrls: ['./primeros-turnos.component.scss'],
})
export class PrimerosTurnosComponent implements OnInit, OnDestroy {
  juegos: any[] = [];
  juegosPrimera: JuegoDTO[] = [];
  juegosMatutina: JuegoDTO[] = [];
  juegosVespertina: JuegoDTO[] = [];
  juegosNocturna: JuegoDTO[] = [];

  changeJuegoIntervalTurnos: any;
  timeChangeTurnosInterval = 1 / 2;

  nextJuego: number;

  fechaTurnoActual?: string | Date;
  horaTurnoActual?: string;
  turnosJuegos = [
    {
      turno: 'PRIMERO',
      hora: '11:30 HS',
    },
    {
      turno: 'MATUTINO',
      hora: '14:00 HS',
    },
    {
      turno: 'VESPERTINO',
      hora: '17:30 HS',
    },
    {
      turno: 'NOCTURNO',
      hora: '21:00 HS',
    },
  ];

  turnoActual?: string | null;
  primerCdad?: number | null;
  primerProv?: number | null;

  getHora?: String;

  constructor(
    private _juegos: JuegoService,
    private _route: Router,
    private _dateService: DateService
  ) {
    this.nextJuego = 0;
  }

  ngOnInit(): void {
    // this.getHora = this._dateService.getHora().toLocaleString();
    this.getJuegos();
  }

  ngOnDestroy(): void {
    clearInterval(this.changeJuegoIntervalTurnos);
  }

  getJuegos() {
    this._juegos.getSoloJuegos().subscribe((data) => {
      this.juegos.push(...data);

      this.juegos.forEach((juego) => {
        if (
          juego.turno.toUpperCase() === 'PRIMERO' ||
          juego.turno.toUpperCase() === 'PRIMERA'
        ) {
          this.juegosPrimera.push(juego);
        } else if (
          juego.turno.toUpperCase() === 'MATUTINO' ||
          juego.turno.toUpperCase() === 'MATUTINA'
        ) {
          this.juegosMatutina.push(juego);
        } else if (
          juego.turno.toUpperCase() === 'VESPERTINO' ||
          juego.turno.toUpperCase() === 'VESPERTINA'
        ) {
          this.juegosVespertina.push(juego);
        } else if (
          juego.turno.toUpperCase() === 'NOCTURNO' ||
          juego.turno.toUpperCase() === 'NOCTURNA'
        ) {
          this.juegosNocturna.push(juego);
        }
      });

      this.juegos = [];

      this.juegos.push(this.juegosPrimera);
      this.juegos.push(this.juegosMatutina);
      this.juegos.push(this.juegosVespertina);
      this.juegos.push(this.juegosNocturna);

      // console.log(typeof this.juegos[1]);

      for (let i of this.juegos) {
        if (this.juegos[0].length === 0) {
          this.turnoActual = this.turnosJuegos[0].turno;
          this.horaTurnoActual = this.turnosJuegos[0].hora;
          this.primerCdad = null;
          this.primerProv = null;
        } else {
          this.getJugada(i, 0);
        }
      }

      this.changeJuegoTurnos();
    });
  }

  changeJuegoTurnos() {
    this.changeJuegoIntervalTurnos = setInterval(() => {
      this.nextJuego++;
      if (this.juegos.length === this.nextJuego) {
        this._route.navigate(['/primeros']);
      }
      if (this.juegos[this.nextJuego].length === 0) {
        this.turnoActual = this.turnosJuegos[this.nextJuego].turno;
        this.horaTurnoActual = this.turnosJuegos[this.nextJuego].hora;
        this.primerCdad = null;
        this.primerProv = null;
      } else {
        for (let j of this.juegos[this.nextJuego]) {
          this.getJugada(j, this.nextJuego);
        }
      }
    }, this.timeChangeTurnosInterval * 60000);
  }

  getJugada(juego: any, index: any) {
    this.turnoActual = juego.turno.toUpperCase();
    this.horaTurnoActual = this.turnosJuegos[index].hora;
    if (
      juego.nombre.toUpperCase().includes('CIUDAD') ||
      juego.nombre.toUpperCase().includes('NACIONAL')
    ) {
      this._juegos.getLastJuego(juego._id!).subscribe((data) => {
        this.primerCdad = data.puesto1;
        this.fechaTurnoActual = data.fecha;
      });
    } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
      this._juegos.getLastJuego(juego._id!).subscribe((data) => {
        this.primerProv = data.puesto1;
      });
    } else {
      console.log('aun no existe ese juego....');
    }
  }
}
