import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/services/date.service';
import { JuegoDTO, JugadasDTO } from 'src/services/interface/api.interface';
import { JuegoService } from 'src/services/juego.service';
import * as moment from 'moment';
import { ActivatedRoute, Params } from '@angular/router';
import { PersonalizadosService } from 'src/services/personalizados.service';
import { PersonalizadosDTO } from 'src/services/interface/personalizados.interface';
import { interval } from 'rxjs';

@Component({
  selector: 'app-veinte',
  templateUrl: './veinte.component.html',
  styleUrls: ['./veinte.component.scss'],
})
export class VeinteComponent implements OnInit {
  juegos: any[] = [];
  juegosParaLive: any[] = [];
  arrLiveJuegos: any[] = [];
  turnoAJugar?: string;

  // ATRIBUTOS VEINTE
  ultimaJugada?: JugadasDTO;
  jugadaVacia?: JugadasDTO;
  jugadaCount?: JugadasDTO;
  juegoCarteleraActual?: JuegoDTO | undefined;
  juegoEnVivo?: JuegoDTO;

  changeJuegoInterval: any;
  checkHourInterval: any;
  checkCarteleraEnVivo: any;
  normalizeTimersInterval: any;
  intervalLive: any;

  refreshCartelera: number = 0.16666666666666666; //cantidad de minutos para refrescar la cartelera
  checkJugadaEnVivo: number = 0.16666666666666666;
  createAndCheckJugadaEnVivo: number = 0.16666666666666666;

  nextJuego: number;
  nextCheckJuego: number;

  diffNextJuegoEnVivo: number;

  isEnVivo: boolean = false;
  isEnVivoFlag: boolean = false;

  nNumerosJugados: number;
  cant: number;
  i: number = 0;
  // FIN ATRIBUTOS VEINTE

  // ATRIBUTOS PRIMEROS
  changeVeinteInterval: any;
  timeChangeVeinteInterval = 0.16666666666666666;

  primerCdadPrevia?: number | null;
  primerCdadPrimera?: number | null;
  primerCdadMatutina?: number | null;
  primerCdadVespertina?: number | null;
  primerCdadNocturna?: number | null;

  primerProvPrevia?: number | null;
  primerProvPrimera?: number | null;
  primerProvMatutina?: number | null;
  primerProvVespertina?: number | null;
  primerProvNocturna?: number | null;

  primerCBAPrevia?: number | null;
  primerCBAPrimera?: number | null;
  primerCBAMatutina?: number | null;
  primerCBAVespertina?: number | null;
  primerCBANocturna?: number | null;

  primerStaFePrevia?: number | null;
  primerStaFePrimera?: number | null;
  primerStaFeMatutina?: number | null;
  primerStaFeVespertina?: number | null;
  primerStaFeNocturna?: number | null;

  primerERioPrevia?: number | null;
  primerERioPrimera?: number | null;
  primerERioMatutina?: number | null;
  primerERioVespertina?: number | null;
  primerERioNocturna?: number | null;

  primerMdeoPrevia?: number | null;
  primerMdeoPrimera?: number | null;
  primerMdeoMatutina?: number | null;
  primerMdeoVespertina?: number | null;
  primerMdeoNocturna?: number | null;
  // FIN ATRIBUTOS PRIMEROS

  // ATRIBUTOS PRIMEROS TURNOS
  juegosTurnos: any[] = [];
  juegosPrevia: JuegoDTO[] = [];
  juegosPrimera: JuegoDTO[] = [];
  juegosMatutina: JuegoDTO[] = [];
  juegosVespertina: JuegoDTO[] = [];
  juegosNocturna: JuegoDTO[] = [];

  changeJuegoIntervalTurnos: any;
  timeChangeTurnosInterval = 0.16666666666666666;

  fechaTurnoActual?: string | Date;
  horaTurnoActual?: string;
  turnosJuegos = [
    {
      turno: 'LA PREVIA',
      hora: '10:15 HS',
    },
    {
      turno: 'PRIMERO',
      hora: '12:00 HS',
    },
    {
      turno: 'MATUTINO',
      hora: '15:00 HS',
    },
    {
      turno: 'VESPERTINO',
      hora: '18:00 HS',
    },
    {
      turno: 'NOCTURNO',
      hora: '21:00 HS',
    },
  ];

  turnoActual?: string | null;
  primerCdad?: number | null;
  primerProv?: number | null;
  primerCBA?: number | null;
  primerStaFe?: number | null;
  primerERio?: number | null;
  primerMdeo?: number | null;
  // FIN ATRIBUTOS PRIMEROS TURNOS

  isVeinte = true;
  isPrimeros = false;
  isPrimerosTurnos = false;
  isFirstLog = true;

  width = 0;
  top = 0;

  timer$: any;
  initTimer = true;

  startTimerBar() {
    this.timer$ = interval(12.5);
    this.timer$.subscribe(
      () => {
        this.width += 0.125;
      }
    );
  }

  constructor(
    private _juegoService: JuegoService,
    private _dateService: DateService,
    private _aRoute: ActivatedRoute,
    private _ps: PersonalizadosService
  ) {
    this.diffNextJuegoEnVivo = 0;
    this.nextJuego = 0;
    this.nextCheckJuego = 0;
    this.nNumerosJugados = 0;
    this.cant = 0;
  }

  ngOnInit(): void {
    this.top = screen.height - 10;    
    this.nextJuego = 0;
    if (this.isFirstLog) {
      let body = document.getElementsByTagName('body')[0];
    
      if (body.classList.value.includes('stylebodypersonalizados')) {
        body.classList.remove("stylebodypersonalizados");
      }
      body.classList.add("stylebody");
      this.getPersonalizada();
      this.nextView = false;
      this.isFirstLog = false;
      this.getJuegos();
    }
    if (this.isVeinte === true) {
      this.changeJuego();
      // this.normalizeTimers();
    } else if (this.isPrimeros === true) {
      this.cambiarVeinte();
      this.getSoloPrimeros();
    } else if (this.isPrimerosTurnos === true) {
      this.getSoloPrimerosTurnos();
    } else if (this.personalizadoUno == true) {
      this.carteleraUno();
    } else if (this.personalizadoDos == true) {
      this.resetCartelera();
    }
  }

  getJuegos() {
    this._juegoService.getSoloJuegos().subscribe((data) => {
      console.log(data);
      if (this.initTimer) {
        this.startTimerBar();
        this.initTimer = false;
      }
      if (!this.isEnVivo) {
        this.juegos = [];
        this.juegosParaLive = [];
        data.forEach((x: any) => {
          if (x.juegoActual) {
            // this.juegos.push(x);
            if (x.turno === 'Primero' || x.turno === 'Vespertina' || x.turno === 'La Previa') {
              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD')) this.juegos[0] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA')) this.juegos[1] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA')) this.juegos[2] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE')) this.juegos[3] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS')) this.juegos[4] = x;
            } else if (x.turno === 'Matutina' || x.turno === 'Nocturna') {
              if (x.nombre?.toLocaleUpperCase().includes('CIUDAD')) this.juegos[0] = x;
              if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA')) this.juegos[1] = x;
              if (x.nombre?.toLocaleUpperCase().includes('CORDOBA')) this.juegos[2] = x;
              if (x.nombre?.toLocaleUpperCase().includes('SANTA FE')) this.juegos[3] = x;
              if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS')) this.juegos[4] = x;
              if (x.nombre?.toLocaleUpperCase().includes('MONTEVIDEO')) this.juegos[5] = x;
            }
          }
          this.juegosParaLive.push(x);
        })
        if (this.isVeinte === true) {
          if (this.juegos.length !== 0) {
            this.juegoCarteleraActual = this.juegos[0];
            this.getUltimaJugada(this.juegos[0]._id!);
          }
          this.checkHour();
        } else if (this.isPrimeros === true) {
        } else if (this.isPrimerosTurnos === true) {
        }
      }
      // this.width = 0;
      // this.timer$
    });
  }

  // METODOS VEINTE
  ngOnDestroyVeinte(): void {
    clearInterval(this.changeJuegoInterval);
    clearInterval(this.checkHourInterval);
    clearInterval(this.checkCarteleraEnVivo);
    clearInterval(this.normalizeTimersInterval);
  }

  getUltimaJugada(id: string) {
    this._juegoService.getLastJuego(id!).subscribe((data) => {
      this.ultimaJugada = data;
      this.width = 0;
    });
  }

  checkNextView() {
    let hora = this._dateService.getHora();
    clearInterval(this.changeJuegoInterval);

    let juegoPrimera: any = null;
    console.log(this.juegosParaLive);
    
    this.juegosParaLive.forEach((x) => {
      if (
        x.horaDelSorteo < hora.toLocaleTimeString() &&
        (x.turno.toUpperCase() === 'LA PREVIA')
      ) {
        juegoPrimera = x;
      } 
    });
    console.log(juegoPrimera);

    if (juegoPrimera != null) {
      this.ngOnDestroyVeinte();
      this.isVeinte = false;
      this.isPrimeros = false;
      this.isPrimerosTurnos = true;
      this.nextView = true;
      this.width = 0;
      this._juegoService.getLastJuego(juegoPrimera!._id).subscribe((x) => {        
        const updateAt = new Date(x.fecha!.toString());
        updateAt.setHours(updateAt.getHours() + 5);
        if (
          parseFloat(
            updateAt!.toString().split(' ')[2]
          ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
        ) {
          this.ngOnDestroyVeinte();
          this.isVeinte = false;
          this.isPrimeros = false;
          this.isPrimerosTurnos = true;
          this.nextView = true;
          this.width = 0;
  
          this.ngOnInit();
        } else {  
           if (this.personalizado === undefined) {
            this.ngOnDestroyVeinte();
            this.isVeinte = true;
            this.isPrimeros = false;
            this.isPrimerosTurnos = false;
            this.personalizadoUno = false;
            this.personalizadoDos = false;
            this.isFirstLog = true;
            this.width = 0;
  
            this.ngOnInit();
          } else {
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.isFirstLog = false;
            this.personalizadoUno = true;
            this.personalizadoDos = false;
            this.width = 0;
  
            this.ngOnInit();
          }
        }
      })
    } else {
      if (this.personalizado === undefined) {
        this.ngOnDestroyVeinte();
        this.isVeinte = true;
        this.isPrimeros = false;
        this.isPrimerosTurnos = false;
        this.personalizadoUno = false;
        this.personalizadoDos = false;
        this.isFirstLog = true;
        this.width = 0;

        this.ngOnInit();
      } else {
        let body = document.getElementsByTagName('body')[0];
        if (body.classList.value.includes('stylebody')) {
          body.classList.remove("stylebody");
        }
        body.classList.add("stylebodypersonalizados");
        this.isVeinte = false;
        this.isPrimerosTurnos = false;
        this.isPrimeros = false;
        this.isFirstLog = false;
        this.personalizadoUno = true;
        this.personalizadoDos = false;
        this.width = 0;

        this.ngOnInit();
      }
    }
  }

  nextView = false;

  changeJuego() {
    this.changeJuegoInterval = setInterval(() => {
      if (!this.isEnVivo) {
        this.nextJuego++;
        if (this.juegos.length === this.nextJuego) {
          this.checkNextView();
        }
        if (this.nextView == false) {
          this.juegoCarteleraActual = this.juegos[this.nextJuego];
          if (this.juegoCarteleraActual !== undefined) {
            this.getUltimaJugada(this.juegoCarteleraActual?._id!);
          }
        }
      }
    }, this.refreshCartelera * 60000);
  }

  checkHour() {
    this.arrLiveJuegos = [];

    //let hora = this._dateService.getHora();    
    let now = moment();
    const date = "2017-03-13";
    console.log('now ' + now.format('HH:mm'));

    this.juegosParaLive.forEach((x) => {      
      this.isEnVivo = false;
      
      let hour = x.horaDelSorteo.toString().split(':')[0];
      // "12:02";
      let minutes = x.horaDelSorteo.toString().split(':')[1];
      let timeToPlay = moment(date + ' ' + hour+':'+minutes);
      let timeToPlayPlusTime = moment(date + ' ' + hour+':'+minutes);
      timeToPlayPlusTime.add(10, 'minutes');

      if (
        now.format('HH:mm') == timeToPlay.format('HH:mm') && new Date().getDay() !== 0 || 
        now.format('HH:mm') <= timeToPlayPlusTime.format('HH:mm') &&
        now.format('HH:mm') >= timeToPlay.format('HH:mm') && new Date().getDay() !== 0
      ) {
        this.turnoAJugar = x.turno;

        if (x.turno === 'Primero' || x.turno === 'Vespertina' || x.turno === 'La Previa') {
          if (x.nombre?.toLocaleUpperCase().includes('CIUDAD')) this.arrLiveJuegos[0] = x;
          if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA')) this.arrLiveJuegos[1] = x;
          if (x.nombre?.toLocaleUpperCase().includes('CORDOBA')) this.arrLiveJuegos[2] = x;
          if (x.nombre?.toLocaleUpperCase().includes('SANTA FE')) this.arrLiveJuegos[3] = x;
          if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS')) this.arrLiveJuegos[4] = x;
        } else if (x.turno === 'Matutina' || x.turno === 'Nocturna') {
          if (x.nombre?.toLocaleUpperCase().includes('CIUDAD')) this.arrLiveJuegos[0] = x;
          if (x.nombre?.toLocaleUpperCase().includes('PROVINCIA')) this.arrLiveJuegos[1] = x;
          if (x.nombre?.toLocaleUpperCase().includes('CORDOBA')) this.arrLiveJuegos[2] = x;
          if (x.nombre?.toLocaleUpperCase().includes('SANTA FE')) this.arrLiveJuegos[3] = x;
          if (x.nombre?.toLocaleUpperCase().includes('ENTRE RIOS')) this.arrLiveJuegos[4] = x;
          if (x.nombre?.toLocaleUpperCase().includes('MONTEVIDEO')) this.arrLiveJuegos[5] = x;
        }
    
        
      } else {
        console.log('ningun juego en vivo, por el momento....');
        return;
      }
    });
    if (this.turnoAJugar) {
      this._juegoService.updateJuegoActualEnCartelera(this.turnoAJugar).subscribe(() => {
        console.log('se modificaron los datos');
      });
    }
    
    this.activeLiveMode();
  }

  activeLiveMode() {
    this.i = 0;
    if (this.arrLiveJuegos.length > 0) {
      this.intervalLive = setInterval(() => {
        if (this.arrLiveJuegos[this.i] !== undefined) {
          clearInterval(this.changeJuegoInterval);
          clearInterval(this.checkHourInterval);

          this._juegoService
            .getLastJuego(this.arrLiveJuegos[this.i]?._id)
            .subscribe((data) => {
              this.width = 0;
              this.ultimaJugada = data;

              this.isEnVivo = true;
              this._juegoService
                .getSoloJuego(this.arrLiveJuegos[this.i]._id!)
                .subscribe((x) => {
                  this.juegoCarteleraActual = x;

                  this.cantNumJugados(
                    this.ultimaJugada!,
                    this.arrLiveJuegos[this.i]._id!,
                    this.juegoCarteleraActual!
                  );
    
                  if (this.arrLiveJuegos.length - 1 === this.i) {
                    this.i = 0;
                  } else {
                    this.i++;
                  }
                });                
            });
        }
      }, this.checkJugadaEnVivo * 60000);
    } else {
      console.log('El array no tiene juegos');
    }
  }

  async cantNumJugados(ultimaJugada: JugadasDTO, id: string, juego: JuegoDTO) {
    this.nNumerosJugados = 0;
    Object.entries(ultimaJugada).forEach(([key, value]) => {
      if (value !== null) {
        this.nNumerosJugados++;
      }
      if (this.nNumerosJugados === 21) {
        this.arrLiveJuegos = this.arrLiveJuegos.filter((x) => x._id !== id);
        this.i = 0;
        if (this.arrLiveJuegos.length === 0) {
          setTimeout(() => {
            this.isEnVivo = false;
            this.ngOnDestroyVeinte();
            this.isVeinte = false;
            this.isPrimeros = true;
            this.isPrimerosTurnos = false;
            this.isFirstLog = true;
            this.width = 0;

            this.ngOnInit();
          }, 10000);
        }
        return this.nNumerosJugados;
      } else {
        let hora = this._dateService.getHora();  
        if ((hora.getHours().toString().length === 1 ? '0' + hora.getHours().toString() : hora.getHours().toString()) 
        === 
        (juego.horaDelSorteo.toString().split(':')[0]).toString()
          &&
          ((hora.getMinutes().toString().length === 1 ? ('0' + hora.getMinutes().toString()) : (hora.getMinutes().toString()))
            >
          ((+(juego.horaDelSorteo.toString().split(':')[1])+15).toString()))) {
            setTimeout(() => {
              try {
                this.resetPage()
              } catch (e) {
                window.location.reload();
              }
            }, 10000);
          }
      }
      return this.nNumerosJugados;
    });
    return this.nNumerosJugados;
  }

  createVoidJugada(): JugadasDTO {
    let jugadaNueva = {
      fecha: moment().format().toString().split('T', 1)[0],
      puesto1: null,
      puesto2: null,
      puesto3: null,
      puesto4: null,
      puesto5: null,
      puesto6: null,
      puesto7: null,
      puesto8: null,
      puesto9: null,
      puesto10: null,
      puesto11: null,
      puesto12: null,
      puesto13: null,
      puesto14: null,
      puesto15: null,
      puesto16: null,
      puesto17: null,
      puesto18: null,
      puesto19: null,
      puesto20: null,
    };
    return jugadaNueva;
  }
  // FIN METODOS VEINTE

  // METODOS PRIMEROS
  getSoloPrimeros() {
    let diaActual = moment().format('L');
    this.juegosParaLive.forEach((juego) => {
      if (juego.turno.toUpperCase() === 'LA PREVIA') {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCdadPrevia = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerProvPrevia = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('CORDOBA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCBAPrevia = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('SANTA FE')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerStaFePrevia = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('ENTRE RIOS')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerERioPrevia = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('MONTEVIDEO')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerMdeoPrevia = data.puesto1;
            } 
          });
        }
      } else if (
        juego.turno.toUpperCase() === 'PRIMERO' ||
        juego.turno.toUpperCase() === 'PRIMERA'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCdadPrimera = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerProvPrimera = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('CORDOBA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCBAPrimera = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('SANTA FE')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerStaFePrimera = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('ENTRE RIOS')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerERioPrimera = data.puesto1;
            }
          });
        } else if (juego.nombre.toUpperCase().includes('MONTEVIDEO')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerMdeoPrimera = data.puesto1;
            } 
          });
        }
      } else if (
        juego.turno.toUpperCase() === 'MATUTINA' ||
        juego.turno.toUpperCase() === 'MATUTINO'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCdadMatutina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerProvMatutina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('CORDOBA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCBAMatutina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('SANTA FE')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerStaFeMatutina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('ENTRE RIOS')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerERioMatutina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('MONTEVIDEO')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerMdeoMatutina = data.puesto1;
            } 
          });
        }
      } else if (
        juego.turno.toUpperCase() === 'VESPERTINA' ||
        juego.turno.toUpperCase() === 'VESPERTINO'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCdadVespertina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerProvVespertina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('CORDOBA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCBAVespertina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('SANTA FE')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerStaFeVespertina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('ENTRE RIOS')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerERioVespertina = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('MONTEVIDEO')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerMdeoVespertina = data.puesto1;
            } 
          });
        }
      } else if (
        juego.turno.toUpperCase() === 'NOCTURNA' ||
        juego.turno.toUpperCase() === 'NOCTURNO'
      ) {
        if (
          juego.nombre.toUpperCase().includes('CIUDAD') ||
          juego.nombre.toUpperCase().includes('NACIONAL')
        ) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCdadNocturna = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerProvNocturna = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('CORDOBA')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerCBANocturna = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('SANTA FE')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerStaFeNocturna = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('ENTRE RIOS')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerERioNocturna = data.puesto1;
            } 
          });
        } else if (juego.nombre.toUpperCase().includes('MONTEVIDEO')) {
          this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
            if (diaActual == moment(data.fecha?.toString().split('.')[0]).format('L')) {
              this.primerMdeoNocturna = data.puesto1;
            } 
          });
        }
      }
    });
  }

  ngOnDestroyPrimeros() {
    clearInterval(this.changeVeinteInterval);
  }

  cambiarVeinte() {
    this.changeVeinteInterval = setInterval(() => {
      if (this.personalizado === undefined) {
        try {
          this.resetPage()
        } catch (e) {
          window.location.reload();
        }
      } else {
        let body = document.getElementsByTagName('body')[0];
        if (body.classList.value.includes('stylebody')) {
          body.classList.remove("stylebody");
        }
        body.classList.add("stylebodypersonalizados");
        this.isVeinte = false;
        this.isPrimerosTurnos = false;
        this.isPrimeros = false;
        this.isFirstLog = false;
        this.personalizadoUno = true;
        this.personalizadoDos = false;
        this.width = 0;

        this.ngOnInit();
      }
    }, this.timeChangeVeinteInterval * 60000);
  }
  // FIN METODOS PRIMEROS

  // METODOS PRIMEROS TURNOS
  getSoloPrimerosTurnos() {
    this.juegosParaLive.forEach((juego) => {
      if (
        juego.turno.toUpperCase() === 'LA PREVIA' 
      ) {
        this.juegosPrevia.push(juego);
      } else if (
        juego.turno.toUpperCase() === 'PRIMERO' ||
        juego.turno.toUpperCase() === 'PRIMERA'
      ) {
        if (
          juego.horaDelSorteo < new Date().toLocaleTimeString() &&
          new Date(juego.updatedAt)
            .toLocaleString()
            .toString()
            .split(' ')[0] === new Date().toLocaleString().split(' ')[0]
        ) {
          this.juegosPrimera.push(juego);
        }
      } else if (
        juego.turno.toUpperCase() === 'MATUTINO' ||
        juego.turno.toUpperCase() === 'MATUTINA'
      ) {
        if (
          juego.horaDelSorteo < new Date().toLocaleTimeString() &&
          new Date(juego.updatedAt)
            .toLocaleString()
            .toString()
            .split(' ')[0] === new Date().toLocaleString().split(' ')[0]
        ) {
          this.juegosMatutina.push(juego);
        }
      } else if (
        juego.turno.toUpperCase() === 'VESPERTINO' ||
        juego.turno.toUpperCase() === 'VESPERTINA'
      ) {
        if (
          juego.horaDelSorteo < new Date().toLocaleTimeString() &&
          new Date(juego.updatedAt)
            .toLocaleString()
            .toString()
            .split(' ')[0] === new Date().toLocaleString().split(' ')[0]
        ) {
          this.juegosVespertina.push(juego);
        }
      } else if (
        juego.turno.toUpperCase() === 'NOCTURNO' ||
        juego.turno.toUpperCase() === 'NOCTURNA'
      ) {
        if (
          juego.horaDelSorteo < new Date().toLocaleTimeString() &&
          new Date(juego.updatedAt)
            .toLocaleString()
            .toString()
            .split(' ')[0] === new Date().toLocaleString().split(' ')[0]
        ) {
          this.juegosNocturna.push(juego);
        }
      }
    });

    this.juegosTurnos.splice(0, this.juegosTurnos.length);

    if (this.juegosPrevia.length !== 0) {
      this.juegosTurnos.push(this.juegosPrevia);
    }
    if (this.juegosPrimera.length !== 0) {
      this.juegosTurnos.push(this.juegosPrimera);
    }
    if (this.juegosMatutina.length !== 0) {
      this.juegosTurnos.push(this.juegosMatutina);
    }
    if (this.juegosVespertina.length !== 0) {
      this.juegosTurnos.push(this.juegosVespertina);
    }
    if (this.juegosNocturna.length !== 0) {
      this.juegosTurnos.push(this.juegosNocturna);
    }

    if (this.juegosTurnos.length !== 0) {
      this.turnoActual = this.turnosJuegos[0].turno;
      this.horaTurnoActual = this.turnosJuegos[0].hora;

      this.primerCdad = null;
      this.primerProv = null;
      this.primerCBA = null;
      this.primerStaFe = null;
      this.primerERio = null;
      this.primerMdeo = null;
      this.getJugada(this.juegosTurnos[0], 0);
    }

    this.changeJuegoTurnos();
  }

  changeJuegoTurnos() {
    if (this.juegosTurnos.length > 0) {
      this.changeJuegoIntervalTurnos = setInterval(() => {
        this.nextJuego++;
        if (this.juegosTurnos.length === this.nextJuego) {
          this.ngOnDestroyPrimerosTurnos();
          this.isVeinte = false;
          this.isPrimerosTurnos = false;
          this.isPrimeros = true;
          this.width = 0;

          this.ngOnInit();
          // this._route.navigate(['/primeros']);
        }
        if (this.juegosTurnos[this.nextJuego].length === 0) {
          // this.turnoActual = this.turnosJuegos[this.nextJuego].turno;
          // this.horaTurnoActual = this.turnosJuegos[this.nextJuego].hora;
          this.primerCdad = null;
          this.primerProv = null;
          this.primerCBA = null;
          this.primerStaFe = null;
          this.primerERio = null;
          this.primerMdeo = null;
          this.width = 0;
        } else {
          this.getJugada(this.juegosTurnos[this.nextJuego], this.nextJuego);
        }
      }, this.timeChangeTurnosInterval * 60000); //
    } else {
      this.ngOnDestroyPrimerosTurnos();
      this.isVeinte = true;
      this.isPrimerosTurnos = false;
      this.isPrimeros = false;
      this.width = 0;

      this.ngOnInit();
    }
  }

  getJugada(juegos: any, index: any) {
    this.width = 0;
    this.primerCdad = null;
    this.primerProv = null;
    this.primerCBA = null;
    this.primerStaFe = null;
    this.primerERio = null;
    this.primerMdeo = null;
    juegos.forEach((juego: JuegoDTO, i: any) => {
      this.turnoActual = juego.turno.toUpperCase();
      this.horaTurnoActual = this.turnosJuegos[index].hora;
      if (
        juego.nombre.toUpperCase().includes('CIUDAD') ||
        juego.nombre.toUpperCase().includes('NACIONAL')
      ) {
        this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
          if (
            parseFloat(
              data.fecha!.toLocaleString().split('-', 4)[2].split('T', 1)[0]
            ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
          ) {
            this.primerCdad = data.puesto1;
            this.fechaTurnoActual = data.fecha;
          } else {
            this.ngOnDestroyPrimerosTurnos();
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.personalizadoUno = true;
            this.width = 0;

            this.ngOnInit();
          }
        });
      } else if (juego.nombre.toUpperCase().includes('PROVINCIA')) {
        this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
          if (
            parseFloat(
              data.fecha!.toLocaleString().split('-', 4)[2].split('T', 1)[0]
            ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
          ) {
            this.primerProv = data.puesto1;
            this.fechaTurnoActual = data.fecha;
          } else {
            this.ngOnDestroyPrimerosTurnos();
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.personalizadoUno = true;
            this.width = 0;

            this.ngOnInit();
          }
        });
      } else if (juego.nombre.toUpperCase().includes('CORDOBA')) {
        this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
          if (
            parseFloat(
              data.fecha!.toLocaleString().split('-', 4)[2].split('T', 1)[0]
            ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
          ) {
            this.primerCBA = data.puesto1;
            this.fechaTurnoActual = data.fecha;
          } else {
            this.ngOnDestroyPrimerosTurnos();
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.personalizadoUno = true;
            this.width = 0;

            this.ngOnInit();
          }
        });
      } else if (juego.nombre.toUpperCase().includes('SANTA FE')) {
        this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
          if (
            parseFloat(
              data.fecha!.toLocaleString().split('-', 4)[2].split('T', 1)[0]
            ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
          ) {
            this.primerStaFe = data.puesto1;
            this.fechaTurnoActual = data.fecha;
          } else {
            this.ngOnDestroyPrimerosTurnos();
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.personalizadoUno = true;
            this.width = 0;

            this.ngOnInit();
          }
        });
      } else if (juego.nombre.toUpperCase().includes('ENTRE RIOS')) {
        this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
          if (
            parseFloat(
              data.fecha!.toLocaleString().split('-', 4)[2].split('T', 1)[0]
            ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
          ) {
            this.primerERio = data.puesto1;
            this.fechaTurnoActual = data.fecha;
          } else {
            this.ngOnDestroyPrimerosTurnos();
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.personalizadoUno = true;
            this.width = 0;

            this.ngOnInit();
          }
        });
      } else if (juego.nombre.toUpperCase().includes('MONTEVIDEO')) {
        this._juegoService.getLastJuego(juego._id!).subscribe((data) => {
          if (
            parseFloat(
              data.fecha!.toLocaleString().split('-', 4)[2].split('T', 1)[0]
            ) === parseFloat(new Date().toLocaleString().split('/', 1)[0])
          ) {
            this.primerMdeo = data.puesto1;
            this.fechaTurnoActual = data.fecha;
          } else {
            this.ngOnDestroyPrimerosTurnos();
            let body = document.getElementsByTagName('body')[0];
            if (body.classList.value.includes('stylebody')) {
              body.classList.remove("stylebody");
            }
            body.classList.add("stylebodypersonalizados");
            this.isVeinte = false;
            this.isPrimerosTurnos = false;
            this.isPrimeros = false;
            this.personalizadoUno = true;
            this.width = 0;

            this.ngOnInit();
          }
        });
      }
    });
  }

  ngOnDestroyPrimerosTurnos(): void {
    clearInterval(this.changeJuegoIntervalTurnos);
  }
  // FIN METODOS PRIMEROS TURNOS

  //Inicio Primer Cartelera Personalizada
  personalizado?: PersonalizadosDTO;
  // INICIO ATRIBUTOS PERSONALIZADOS UNO
  personalizadoUno = false;
  changePersonalizadoUnoInterval: any;
  timeChangePersonalizadoUnoInterval = 0.16666666666666666;
  // FIN ATRIBUTOS PERSONALIZADOS UNO

  // INICIO ATRIBUTOS PERSONALIZADOS DOS
  personalizadoDos = false;
  changePersonalizadoDosInterval: any;
  timeChangePersonalizadoDosInterval = 0.16666666666666666;
  // FIN ATRIBUTO PERSONALIZADOS DOS


  getPersonalizada() {
    let id: string;
    this._aRoute.queryParams.subscribe((params: Params) => id = params['id']);

    this._ps.getPersonalizadoByParametroId(id!).subscribe({
      next: (d) => {
        this.personalizado = d[0];
      },
      error: () => {
        this.personalizado = undefined;
      }
    });
  }

  carteleraUno() {
    if (this.personalizado?.tituloDosCarteleraDos == null && this.personalizado?.tituloDosCarteleraDos == null 
       || this.personalizado?.tituloDosCarteleraDos.length == 0 && this.personalizado?.tituloDosCarteleraDos.length == 0
      ) {
      try {
        this.resetPage();
      } catch (e) {
        window.location.reload();
      }
    } else {
      this.changePersonalizadoUnoInterval = setTimeout(() => {
        if (this.personalizadoUno == true) {
          this.isVeinte = false;
          this.isPrimerosTurnos = false;
          this.isPrimeros = false;
          this.isFirstLog = false;
          this.personalizadoUno = false;
          this.personalizadoDos = true;
          this.width = 0;
  
          this.ngOnInit();
        }
      }, this.timeChangePersonalizadoUnoInterval * 60000);
    }
  }

  resetCartelera() {
    clearTimeout(this.changePersonalizadoUnoInterval);
    try {
      this.changePersonalizadoDosInterval = setTimeout(() => {
        this.width = 0;
        this.resetPage();
      }, this.timeChangePersonalizadoDosInterval * 60000);
    } catch (e) {
      window.location.reload();
    }
  }
  //Fin Primer Cartelera Personalizada

  // Para resetear la pagina
  private resetPage() {
    this.juegos = [];
    this.juegosParaLive = [];
    this.arrLiveJuegos = [];
    this.turnoAJugar = undefined;

    // ATRIBUTOS VEINTE
    this.ultimaJugada = undefined;
    this.jugadaVacia = undefined;
    this.jugadaCount = undefined;
    this.juegoCarteleraActual = undefined;
    this.juegoEnVivo = undefined;

    this.diffNextJuegoEnVivo = 0;
    this.nextJuego = 0;
    this.nextCheckJuego = 0;
    this.nNumerosJugados = 0;
    this.cant = 0;

    this.isEnVivo = false;
    this.isEnVivoFlag = false;

    this.cant = 0;
    this.i = 0;
    // FIN ATRIBUTOS VEINTE

    // ATRIBUTOS PRIMEROS
    this.primerCdadPrevia = null;
    this.primerCdadPrimera = null;
    this.primerCdadMatutina = null;
    this.primerCdadVespertina = null;
    this.primerCdadNocturna = null;

    this.primerProvPrevia = null;
    this.primerProvPrimera = null;
    this.primerProvMatutina = null;
    this.primerProvVespertina = null;
    this.primerProvNocturna = null;

    this.primerCBAPrevia = null;
    this.primerCBAPrimera = null;
    this.primerCBAMatutina = null;
    this.primerCBAVespertina = null;
    this.primerCBANocturna = null;

    this.primerStaFePrevia = null;
    this.primerStaFePrimera = null;
    this.primerStaFeMatutina = null;
    this.primerStaFeVespertina = null;
    this.primerStaFeNocturna = null;

    this.primerERioPrevia = null;
    this.primerERioPrimera = null;
    this.primerERioMatutina = null;
    this.primerERioVespertina = null;
    this.primerERioNocturna = null;

    this.primerMdeoPrevia = null;
    this.primerMdeoPrimera = null;
    this.primerMdeoMatutina = null;
    this.primerMdeoVespertina = null;
    this.primerMdeoNocturna = null;
    // FIN ATRIBUTOS PRIMEROS

    // ATRIBUTOS PRIMEROS TURNOS
    this.juegosTurnos = [];
    this.juegosPrevia = [];
    this.juegosPrimera = [];
    this.juegosMatutina = [];
    this.juegosVespertina = [];
    this.juegosNocturna = [];

    this.fechaTurnoActual = undefined;
    this.horaTurnoActual = undefined;

    this.turnoActual = null;
    this.primerCdad = null;
    this.primerProv = null;
    this.primerCBA = null;
    this.primerStaFe = null;
    this.primerERio = null;
    this.primerMdeo = null;
    // FIN ATRIBUTOS PRIMEROS TURNOS

    this.isVeinte = true;
    this.isPrimerosTurnos = false;
    this.isPrimeros = false;
    this.isFirstLog = true;
    this.personalizadoUno = false;
    this.personalizadoDos = false;
    this.nextView = false;

    this.width = 0;
    this.top = 0;

    clearInterval(this.intervalLive);
    clearInterval(this.changeJuegoInterval);
    clearInterval(this.changeVeinteInterval);
    clearInterval(this.changeJuegoIntervalTurnos);

    this.ngOnInit();
  }
}
