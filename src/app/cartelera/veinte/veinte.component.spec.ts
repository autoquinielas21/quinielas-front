import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VeinteComponent } from './veinte.component';

describe('VeinteComponent', () => {
  let component: VeinteComponent;
  let fixture: ComponentFixture<VeinteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VeinteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VeinteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
