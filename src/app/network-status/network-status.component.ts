import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-network-status',
  templateUrl: './network-status.component.html',
  styleUrls: ['./network-status.component.scss']
})
export class NetworkStatusComponent implements OnInit {

  netStatus: string;
  left = 0;
  top = 0;

  constructor() {
    this.netStatus = '';
  }

  ngOnInit(): void {
    this.top = screen.height - (screen.height - 20); 
    this.left = screen.width - 460;
    this.timer();
  }

  timer() {
    let networkStatusInterval = setInterval(() => {
      this.netStatus = this.detectNetworkStatus();
      if (this.netStatus === 'Offline') {
        clearInterval(networkStatusInterval);
        let offlineInterval = setInterval(() => {
          this.netStatus = this.detectNetworkStatus();
          if (this.netStatus === 'Online') {
            clearInterval(offlineInterval);
            window.location.reload();
          }
        }, 6000);
      }
    }, 5000);
  }

  detectNetworkStatus(): string {
    if (navigator.onLine) {
      return 'Online';
    } else {
      return 'Offline';
    }
  }

}
